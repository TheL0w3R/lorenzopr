import Vue from 'vue';
import Vuex from 'vuex';
import mutations from './mutations';
import getters from './getters';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  state: {
    menuState: false,
    selected: '#header',
    modal: false,
    modalData: {
      title: 'Error',
      body: 'Not Found',
      footer: [],
    },
    alert: false,
    alertData: {
      title: 'Error',
      body: 'Not Found',
    },
  },
  getters,
  mutations,
  strict: debug,
});
