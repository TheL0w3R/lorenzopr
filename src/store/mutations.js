function lockScroll() {
  const el = document.getElementById('body');
  if (el.classList) el.classList.add('modal-open');
  else el.className += 'modal-open';
}

function unlockScroll() {
  const el = document.getElementById('body');
  const className = 'modal-open';
  if (el.classList) el.classList.remove('modal-open');
  else el.className = el.className.replace(new RegExp(`(^|\\b) ${className.split(' ').join('|')} (\\b|$)`, 'gi'), ' ');
}

export default {
  toggleMenu(state) {
    state.menuState = !state.menuState;
  },
  setMenuState(state, mState) {
    state.menuState = mState;
  },
  setSelected(state, option) {
    state.selected = option;
  },
  openModal(state, data) {
    state.modal = true;
    if (data !== undefined) {
      lockScroll();
      state.modalData = {
        ...state.modalData, ...data,
      };
    }
  },
  closeModal(state) {
    unlockScroll();
    state.modal = false;
  },
  openAlert(state, data) {
    state.alert = true;
    if (data !== undefined) {
      lockScroll();
      state.alertData = {
        ...state.alertData, ...data,
      };
    }
  },
  closeAlert(state) {
    unlockScroll();
    state.alert = false;
  },
};
