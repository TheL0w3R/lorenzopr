export default {
  getMenuState(state) {
    return state.menuState;
  },
  getSelected(state) {
    return state.selected;
  },
  getModalState(state) {
    return state.modal;
  },
  getModalData(state) {
    return state.modalData;
  },
  getAlertState(state) {
    return state.alert;
  },
  getAlertData(state) {
    return state.alertData;
  },
};
