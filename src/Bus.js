import tlgram from '@/assets/img/tlgram.png';
import HPW from '@/assets/img/HPW LOGO 3.png';
import lpr from '@/assets/img/lorenzopr logo.png';

export default [
  {
    title: 'lorenzopr',
    icon: lpr,
    tech: 'VueJS, HTML/CSS',
    body: `
      <img class="project_icon" src="${lpr}" alt="">
      <h2>About lorenzopr</h2>
      <p>lorenzopr is my personal website, the one you're looking at right now.</p>
      <p>Built with the purpose of promoting myself, showing my web development skills and my personal projects.</p>
      <p>It's written in Javascript, using the <a href="https://vuejs.org">VueJS</a> Framework and the SASS preprocessor for CSS.</p>
      <p class="license">License: <span class="bold">Open Source</span></p>`,
    footer: [
      {
        text: 'Visit',
        href: 'https://lorenzopr.com/',
        isAlt: false,
      },
      {
        text: 'Repo',
        href: 'https://bitbucket.org/TheL0w3R/lorenzopr',
        isAlt: true,
      },
    ],
  },
  {
    title: 'TLGram',
    icon: tlgram,
    tech: 'ReactJS, Firebase',
    body: `
      <img class="project_icon" src="${tlgram}" alt="">
      <h2>About TLGram</h2>
      <p>TLGram is a clone of Instagram made for learning purposes.</p>
      <p>Is written in Javascript using <a href="https://reactjs.org">ReactJS</a> Library and <a href="https://material-ui-next.com">Material-UI</a> for it's presentation components,
      although some of them are custom made too.</p>
      <p>For it's backend, it's currently using Firebase and Firebase Realtime Database.</p>
      <p>It currently packs features like:</p>
      <ul>
        <li>Registration</li>
        <li>Logging In</li>
        <li>Uploading photos</li>
        <li>Searching other users</li>
        <li>Viewing users profiles</li>
        <li>More features coming...</li>
      </ul>
      <p class="license">License: <span class="bold">Open Source</span></p>`,
    footer: [
      {
        text: 'Visit',
        href: 'https://tlgram.lorenzopr.com/',
        isAlt: false,
      },
      {
        text: 'Repo',
        href: 'https://bitbucket.org/TheL0w3R/tlgram',
        isAlt: true,
      },
    ],
  },
  {
    title: 'HPWizard',
    icon: HPW,
    tech: 'Java',
    body: `
      <img class="project_icon" src="${HPW}" alt="">
      <h2>About HPWizard</h2>
      <p>HPWizard is a role-playing plugin for Spigot. Is written in Java and using the Bukkit API.</p>
      <p>Some of it's features are:</p>
      <ul>
        <li>Wands</li>
        <li>Spells</li>
        <li>Brooms</li>
        <li>Floo Network</li>
        <li>Sorting Hat</li>
        <li>House points</li>
        <li>Custom house colors</li>
        <li>Custom house names</li>
        <li>House point signs</li>
        <li>Custom spell sounds</li>
      </ul>
      <h2>Showcase Video</h2>
      <div class="yt-video"><iframe src="https://www.youtube.com/embed/KgWedADcPm4" frameborder="0" allow="encrypted-media" allowfullscreen></iframe></div>
      <p>You can read the full description by visiting the project link below.</p>
      <p class="license">License: <span class="bold">Commercial</span></p>`,
    footer: [
      {
        text: 'Visit',
        href: 'https://www.spigotmc.org/resources/hpwizard.26821/',
        isAlt: false,
      },
    ],
  },
];
