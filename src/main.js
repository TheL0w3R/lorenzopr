// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'normalize.css';
import '@/styles/fontawesome-all.min.css';
import Vue from 'vue';
import VueCarousel from 'vue-carousel';
import VueScrollTo from 'vue-scrollto';
import store from './store';
import App from './App';

import '../node_modules/bootstrap/dist/css/bootstrap-grid.min.css';

Vue.config.productionTip = false;

Vue.use(VueCarousel);
Vue.use(VueScrollTo);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  components: { App },
  template: '<App/>',
});
